import { DataService } from './data.service';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookiesService } from './cookies.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CorporationService {

    public corporations = [];
    public corporationId = null;
    public corporationsWebSites = [];
    public filteredCorporationsWebSites = [];
    public corporationWebsiteId = null;

    constructor(private dataService: DataService,
        private cookiesService: CookiesService) {
        this.corporations = [{ corporationId: null, corporationName: "Waiting for server..." }];
        this.filteredCorporationsWebSites = [{ corporationWebsiteId: null, websiteName: "Waiting for server...", urlBase: null }];
        this.corporationWebsiteId = this.filteredCorporationsWebSites[0].CorporationWebsiteId;
    }

    public setOptions(corporations, webSites) {
        this.corporations = corporations;
        this.corporationsWebSites = webSites;
        this.corporationId = this.corporations[0] ? this.corporations[0].corporationId : null;
        this.filterCorporationsWebSitesByCorporationId();
    }

    public filterCorporationsWebSitesByCorporationId() {

        this.filteredCorporationsWebSites = this.corporationsWebSites.filter((webSite) => {
            if (webSite.corporationId == this.corporationId)
                return true;
            return false;
        })

        if (this.filteredCorporationsWebSites.length > 0)
            this.corporationWebsiteId = this.filteredCorporationsWebSites[0].corporationWebsiteId;
        else
            this.corporationWebsiteId = null;
    }

    public getCorporationsData(): Observable<any> {
        return this.dataService.post('auth/get-corporations', {}).pipe(map((result) => {
            this.setOptions(result[0], result[1]);
            // set selection from cookies
            let data: any = this.cookiesService.getCookie('backendConnection');
            if (data) {
                data = JSON.parse(data);
                this.corporationId = data.corporationId;
                this.filterCorporationsWebSitesByCorporationId();
                for (let website of this.corporationsWebSites) {
                    if (website.corporationWebsiteId == data.corporationWebsiteId) {
                        this.corporationWebsiteId = website.corporationWebsiteId;
                        break;
                    }
                }
            }
        }));
    }
}