import { Observable } from 'rxjs';
import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { User } from '../interfaces/general';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: User = null;

  constructor(private dataService: DataService) {
    let x = localStorage.getItem('currentUser');
    if (x)
      this.currentUser = JSON.parse(x);
  }

  login(loginData: { userName: string, password: string }): Observable<any> {
    return this.dataService.post('auth/login', loginData);
  }
}
