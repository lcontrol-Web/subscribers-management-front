import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { EnvService } from './env/env.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseUrl: string;

  constructor(private http: HttpClient,
    private envService: EnvService) {
    this.baseUrl = this.envService.apiUrl;
  }


  public get(path): Observable<any> {
    return this.http.get(this.baseUrl + path);
  }

  public getByParam(path, param): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(this.baseUrl + '/' + path + '/' + param, httpOptions);
  }

  public post(path, data): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post(this.envService.apiUrl + path, JSON.stringify(data), httpOptions);
  }
}
