import { User } from './general';

export interface LoginUser {
    Token: string;
    LoginUser: User;
    Guid: string;
}