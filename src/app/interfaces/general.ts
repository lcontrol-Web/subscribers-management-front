export interface User {
    userId: number;
    userName: string;
    fullName: string;
    password: string;
    email: string;
    phone: string;
    allowedToCreateUser: number;
    allowedToCreateSubscriber: number;
    allowedToUpdateSubscriber: number;
    allowedToGenerateLicense: number;
}

export interface Subscriber {
    subscriberId: number;
    macAddress: string;
    subscriptionDate: string | Date;
    email: string;
    fullName: string;
    phone: string;
    lastExperationDate?: string; // exists only on website
    lastSerialNumber?: string; // exists only on website
    status?: string; // exists only on website
}

export interface Purchase {
    purchaseId: number;
    subscriberId: number;
    purchaseDate: Date | string;
    experationDate: Date | string;
    cost: number;
    currency: string;
}

export interface SerialNumberActivation {
    serialNumberId: number;
    serialNumber: string;
    activationDate: string | Date;
    subscriberId: number;
    decodedLicense?: {
        macAddress: string,
        doors: number,
        cameras: number,
        lprs: number,
        zones: number,
        fingerprints: number,
        clients: number,
        tags: number,
        fireDetectors: number,
        fingerPrints: number,
        faceRecognitions: number
    }
}

export interface ActionLogs {
    logId: number,
    userId: number,
    userName: string,
    action: string,
    description: string,
    date: string
}