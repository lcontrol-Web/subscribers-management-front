import { AuthService } from './../../services/auth.service';
import { Subscriber, Purchase, SerialNumberActivation } from './../../interfaces/general';
import { Component, OnInit } from '@angular/core';
import { SubscribersService } from './subscribers.service';
import { forkJoin } from 'rxjs';
import { MessageService } from 'primeng/api';

declare var $: any;

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.css']
})
export class SubscribersComponent implements OnInit {

  public dataIsReady = false;
  public loadingTable = true;

  subscribers: Subscriber[] = [];
  manipulatedTable: Subscriber[] = [];
  subscriberToEdit: Subscriber = null;
  modalType: string = null;

  newPurchase: Purchase = <any>{};
  purchasesHistory: Purchase[] = [];

  newSerialNumberActivation: SerialNumberActivation = <any>{};
  serialNumberHistory: SerialNumberActivation[] = [];

  searchActive = false;

  /* filters */
  filters = {
    fullName: '',
    email: '',
    phone: '',
    lastExperationDate: '',
    status: '',
    macAddress: '',
    lastSerialNumber: ''
  }

  /* sorts */
  sorts = {
    fullName: 0,
    email: 0,
    phone: 0,
    lastExperationDate: 0,
    macAddress: 0,
    serialNumber: 0
  }

  currencies = ['₪', '$', '€', '¥'];

  bundleOptions = [
    { text: 'unlimited', value: 1200 },
    { text: '1 Month', value: 1 },
    { text: '3 Months', value: 3 },
    { text: '6 Months', value: 6 },
    { text: '12 Months', value: 12 },
    { text: '18 Months', value: 18 },
    { text: '2 Years', value: 24 },
    { text: '3 Years', value: 36 },
    { text: '5 Years', value: 60 },
  ];

  selectedBundleOption = 3;

  public loadingRequest = false;
  public macAddressAlreadyExists = false;
  public newPurchaseMode = false; // only for 'Edit' mode
  public newSrialNumberMod = false; // only for 'Edit' mode
  public generateSerialNumber = false;

  public decodedLicense = {
    macAddress: '',
    clients: 0,
    doors: 0,
    cameras: 0,
    zones: 0,
    lprs: 0,
    tags: 0,
    fireDetectors: 0,
    fingerPrints: 0,
    faceRecognitions: 0
  };

  public showFullLicenseKey = '';

  constructor(private subscribersService: SubscribersService, private messageService: MessageService, public authService: AuthService) { }

  ngOnInit() {
    this.subscribersService.getSubscribers().subscribe(res => {
      this.subscribers = res;
      this.manipulatedTable = this.subscribers;
      this.loadingTable = false;
    });
  }

  public onOpenModal(type: 'Add' | 'Edit' | 'Delete', subscriber?: Subscriber) {

    if (!this.authAction(type))
      return;

    this.dataIsReady = false;
    $('#subscriberModal').modal('show');
    this.modalType = type;
    this.newPurchaseMode = false;
    this.newSrialNumberMod = false;
    this.generateSerialNumber = false;
    this.showFullLicenseKey = '';

    this.subscriberToEdit = subscriber ? copy(subscriber) : {
      subscriberId: null,
      fullName: null,
      phone: null,
      email: null
    };

    this.resetNewPurchase();
    this.resetNumOf();

    if (this.modalType == 'Edit') {

      this.getPurchasesHistory();
    }
    else
      this.dataIsReady = true;
  }

  private resetNewPurchase() {
    this.newPurchase = {
      purchaseId: null,
      purchaseDate: new Date(),
      experationDate: null,
      cost: 0,
      currency: '₪',
      subscriberId: null
    };

    this.newSerialNumberActivation = {
      serialNumberId: null,
      serialNumber: null,
      activationDate: null,
      subscriberId: null
    }

    this.decodedLicense = {
      macAddress: '',
      clients: 0,
      doors: 0,
      cameras: 0,
      zones: 0,
      lprs: 0,
      tags: 0,
      fireDetectors: 0,
      fingerPrints: 0,
      faceRecognitions: 0,
    }
  }

  public onSubmit() {

    if (this.modalType == 'Edit' && !this.authService.currentUser.allowedToUpdateSubscriber)
      return false;

    this.loadingRequest = true;
    this.macAddressAlreadyExists = false;

    if (this.modalType == 'Add') {

      this.subscriberToEdit.subscriptionDate = dateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss');

      // create subscription
      this.subscribersService.addSubscriber(this.subscriberToEdit).subscribe(res => {

        this.subscribers.splice(0, 0, res);
        // set the new Id
        this.newPurchase.subscriberId = res.subscriberId;
        this.newSerialNumberActivation.subscriberId = res.subscriberId;
        this.newSerialNumberActivation.activationDate = dateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss');

        // get experation date
        this.newPurchase.experationDate = this.calculateExperationDate(dateFormat(this.newPurchase.purchaseDate, 'Date', 'Date'), this.selectedBundleOption);

        // change to DB format
        this.newPurchase.experationDate = dateFormat(this.newPurchase.experationDate, 'Date', 'yyyy-MM-dd HH:mm:ss');
        this.newPurchase.purchaseDate = dateFormat(this.newPurchase.purchaseDate, 'Date', 'yyyy-MM-dd HH:mm:ss');

        let httpReqArr = [];

        httpReqArr.push(this.subscribersService.newPurchase(this.newPurchase));
        httpReqArr.push(this.subscribersService.activateSerialNumber(this.newSerialNumberActivation));

        forkJoin(httpReqArr).subscribe(r => {
          /* handle support purchase */
          let supportPurchaseRes = <Purchase>r[0];
          res.lastExperationDate = <any>supportPurchaseRes.experationDate;

          let today = new Date();

          if (!res.lastExperationDate)
            res.status = 'Not purchased';

          else if (new Date(res.lastExperationDate).getTime() < today.getTime())
            res.status = 'Expired';

          else {
            let preMonth = new Date(res.lastExperationDate);
            preMonth.setMonth(preMonth.getMonth() - 1);

            if (preMonth.getTime() < today.getTime())
              res.status = 'Experation Alert';

            else
              res.status = 'Active';
          }

          /* Handle SerialNumber */
          let serialNumberRes = <SerialNumberActivation>r[1];
          res.lastSerialNumber = serialNumberRes.serialNumber;

          $('#subscriberModal').modal('hide');
        });
      }, (ex) => {
        if (ex.error.indexOf('macAddress_UNIQUE') != -1) {
          this.macAddressAlreadyExists = true;
          $('.modal-body').scrollTop(0);
        }
        console.log(ex.error);
      });
    }
    else if (this.modalType == 'Edit') {
      this.subscribersService.editSubscriber(this.subscriberToEdit).subscribe(res => {
        // update in table
        for (let i = 0; i < this.subscribers.length; i++) {
          if (this.subscribers[i].subscriberId == res.subscriberId) {
            this.subscribers.splice(i, 1, res);
            break;
          }
        }

        // create new purchase if needed
        if (this.newPurchaseMode || this.newSrialNumberMod) {
          if (this.newPurchaseMode) {
            this.newPurchase.subscriberId = this.subscriberToEdit.subscriberId;

            // get experation date
            this.newPurchase.experationDate = this.calculateExperationDate(dateFormat(this.newPurchase.purchaseDate, 'Date', 'Date'), this.selectedBundleOption);

            // change to DB format
            this.newPurchase.experationDate = dateFormat(this.newPurchase.experationDate, 'Date', 'yyyy-MM-dd HH:mm:ss');
            this.newPurchase.purchaseDate = dateFormat(this.newPurchase.purchaseDate, 'Date', 'yyyy-MM-dd HH:mm:ss');

            this.subscribersService.newPurchase(this.newPurchase).subscribe(r => {
              res.lastExperationDate = <any>r.experationDate;

              let today = new Date();

              if (!res.lastExperationDate)
                res.status = 'Not purchased';

              else if (new Date(res.lastExperationDate).getTime() < today.getTime())
                res.status = 'Expired';

              else {
                let preMonth = new Date(res.lastExperationDate);
                preMonth.setMonth(preMonth.getMonth() - 1);

                if (preMonth.getTime() < today.getTime())
                  res.status = 'Experation Alert';

                else
                  res.status = 'Active';
              }

              $('#subscriberModal').modal('hide');
            });
          }
          if (this.newSrialNumberMod) {
            this.newSerialNumberActivation.subscriberId = res.subscriberId;
            this.newSerialNumberActivation.activationDate = dateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss');

            this.subscribersService.activateSerialNumber(this.newSerialNumberActivation).subscribe(r => {
              res.lastSerialNumber = r.serialNumber;

              $('#subscriberModal').modal('hide');
            });
          }
        }
        else
          $('#subscriberModal').modal('hide');

      }, (ex) => {
        if (ex.error.indexOf('macAddress_UNIQUE') != -1) {
          this.macAddressAlreadyExists = true;
          $('.modal-body').scrollTop(0);
        }
        console.log(ex.error);
      });
    }
    else {
      this.subscribersService.deleteSubscriber({ subscriberId: this.subscriberToEdit.subscriberId }).subscribe(res => {
        for (let i = 0; i < this.subscribers.length; i++) {
          if (this.subscribers[i].subscriberId == this.subscriberToEdit.subscriberId) {
            this.subscribers.splice(i, 1);
            break;
          }
        }
        $('#subscriberModal').modal('hide');
      });
    }
  }

  onToggleSort(sortType) {
    // reset all others
    for (let sort of Object.keys(this.sorts))
      if (sort != sortType)
        this.sorts[sort] = 0;

    // toggle selected sort
    if (this.sorts[sortType] == 0)
      this.sorts[sortType] = 1;

    else if (this.sorts[sortType] == 1)
      this.sorts[sortType] = -1;

    else if (this.sorts[sortType] == -1)
      this.sorts[sortType] = 0;

    // apply sort
    this.manipulatedTable.sort((a, b) => {
      if (!a[sortType])
        return 1;
      if (!b[sortType])
        return -1;

      if (a[sortType].toString().toLowerCase() >= b[sortType].toString().toLowerCase())
        return this.sorts[sortType];
      return -1 * this.sorts[sortType];
    });
  }

  filterBy() {
    this.manipulatedTable = this.subscribers.filter(i => {
      for (let filterType of Object.keys(this.filters)) {
        if (this.filters[filterType] && (!i[filterType] || i[filterType].toLowerCase().indexOf(this.filters[filterType].toLowerCase()) == -1))
          return false;
      }

      return true;
    });
  }

  private calculateExperationDate(startDate: Date, months: number): Date {
    let experationDate = new Date(startDate);
    experationDate.setMonth(experationDate.getMonth() + months);

    return experationDate;
  }

  private getPurchasesHistory() {
    let httpReqArr = [];

    httpReqArr.push(this.subscribersService.getPurchaseHistory(this.subscriberToEdit.subscriberId));
    httpReqArr.push(this.subscribersService.getSerialNumbersHistory(this.subscriberToEdit.subscriberId));

    forkJoin(httpReqArr).subscribe(res => {
      this.purchasesHistory = res[0];
      this.serialNumberHistory = res[1];
      this.dataIsReady = true;
    })
  }

  licenseData = {
    macAddress: '',
    numOfClients: 0,
    numOfDoors: 0,
    numOfCameras: 0,
    numOfZones: 0,
    numOfLprs: 0,
    numOfTags: 0,
    numOfFireDetectors: 0,
    numOfFingerprints: 0,
    numOfFaceRecognitions: 0
  }

  public openGenerateSerialNumber() {
    this.generateSerialNumber = true;
    this.resetNumOf();
  }

  private resetNumOf() {
    this.licenseData.macAddress = this.subscriberToEdit.macAddress || '';
    this.licenseData.numOfClients = 0;
    this.licenseData.numOfDoors = 0;
    this.licenseData.numOfCameras = 0;
    this.licenseData.numOfZones = 0;
    this.licenseData.numOfLprs = 0;
    this.licenseData.numOfTags = 0;
    this.licenseData.numOfFireDetectors = 0;
    this.licenseData.numOfFingerprints = 0;
    this.licenseData.numOfFaceRecognitions = 0;
  }

  public generateNewSerialNumber() {

    this.subscribersService.generateNewLicense(this.licenseData).subscribe(res => {
      this.newSerialNumberActivation.serialNumber = res.licenseKey;
      this.newSerialNumberActivation.serialNumber.length;
      this.generateSerialNumber = false;
    });
  }

  public copyLicenseToClipboard(license) {
    $(document).ready(() => {
      /* Get the text field */
      if (this.copyToClipboard($("#fullLicenseToCopy")[0]))
        this.messageService.add({ severity: 'info', summary: 'Info Message', detail: 'Copied License to Clipboard. Use Ctrl+v to paste.', life: 7000 });


    });
  }

  private copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    let target = null
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
      // can just use the original source element for the selection and copy
      target = elem;
      origSelectionStart = elem.selectionStart;
      origSelectionEnd = elem.selectionEnd;
    } else {
      // must use a temporary form element for the selection and copy
      target = document.getElementById(targetId);
      if (!target) {
        target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.id = targetId;
        document.body.appendChild(target);
      }
      target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
      succeed = document.execCommand("copy");
    } catch (e) {
      succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof (<any>currentFocus).focus === "function") {
      (<any>currentFocus).focus();
    }

    if (isInput) {
      // restore prior selection
      elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
      // clear temporary content
      target.textContent = "";
    }
    return succeed;
  }

  authAction(action) {
    if (action == 'Delete' && !this.authService.currentUser.allowedToUpdateSubscriber)
      return false;

    if (action == 'Add' && !this.authService.currentUser.allowedToCreateSubscriber)
      return false;

    return true;
  }
}
