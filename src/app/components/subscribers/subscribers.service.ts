import { Subscriber, Purchase, SerialNumberActivation } from './../../interfaces/general';
import { DataService } from './../../services/data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SubscribersService {

  constructor(private dataService: DataService, private authService: AuthService) { }

  public getSubscribers(): Observable<Subscriber[]> {
    return this.dataService.post('subscribers/get-subscribers', {});
  }

  public addSubscriber(subscriber: Subscriber): Observable<Subscriber> {
    subscriber['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('subscribers/add-subscriber', subscriber)
  }

  public editSubscriber(subscriber: Subscriber): Observable<Subscriber> {
    subscriber['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('subscribers/edit-subscriber', subscriber)
  }

  public deleteSubscriber(subscriber: { subscriberId: number }): Observable<any> {
    subscriber['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('subscribers/delete-subscriber', subscriber)
  }

  public newPurchase(purchase: Purchase): Observable<Purchase> {
    purchase['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('subscribers/new-purchase', purchase);
  }

  public getPurchaseHistory(subscriberId: number): Observable<Array<Purchase>> {
    return this.dataService.post('subscribers/get-purchases-history', { subscriberId: subscriberId });
  }

  public activateSerialNumber(serialNumberData: SerialNumberActivation): Observable<SerialNumberActivation> {
    serialNumberData['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('subscribers/activate-serial-number', serialNumberData);
  }

  public getSerialNumbersHistory(subscriberId: number): Observable<Array<SerialNumberActivation>> {
    return this.dataService.post('subscribers/get-serial-numbers-history', { subscriberId: subscriberId });
  }

  public generateNewLicense(licenseData){
    return this.dataService.post('subscribers/generate-license-key', licenseData);
  }
}
