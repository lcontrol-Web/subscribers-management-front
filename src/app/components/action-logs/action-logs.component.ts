import { ActionLogsService } from './action-logs.service';
import { Component, OnInit } from '@angular/core';
import { ActionLogs } from 'src/app/interfaces/general';

declare var $:any;

@Component({
  selector: 'app-action-logs',
  templateUrl: './action-logs.component.html',
  styleUrls: ['./action-logs.component.css']
})
export class ActionLogsComponent implements OnInit {

  loadingTable = true;

  logs: ActionLogs[] = [];
  logsTable: ActionLogs[] = [];
  manipulatedTable: ActionLogs[] = [];
  searchActive = false;

  /* filters */
  filters = {
    userName: '',
    description: '',
    date: '',
    action: ''
  }

  currentP: number = 0;
  lastP: number = 0;
  itemsPerP: number = 20;

  constructor(private actionLogsService: ActionLogsService) { }

  ngOnInit() {
    this.actionLogsService.getLogs().subscribe(res => {
      this.logs = res;
      this.manipulatedTable = this.logs;
      this.loadingTable = false;

      this.updateTable();
    });
  }

  updateTable() {
    this.logsTable.length = 0;
    for (let i = this.itemsPerP * this.currentP; i < this.itemsPerP * this.currentP + this.itemsPerP; i++) {
      if (this.manipulatedTable[i])
        this.logsTable.push(this.manipulatedTable[i]);
    }
  }

  changeP(direction: number, extreme?: number) {
    let oldP = this.currentP;
    if (!extreme) {
      if ((direction == 1 && (this.currentP + direction < Math.ceil(this.manipulatedTable.length / this.itemsPerP))) ||
        (direction == -1 && this.currentP != 0))
        this.currentP += direction;
    }

    else {
      if (direction == 1)
        this.currentP = Math.ceil(this.manipulatedTable.length / this.itemsPerP) - 1;
      else
        this.currentP = 0;
    }

    if (oldP != this.currentP)
      this.updateTable();
  }

  filterBy() {
    this.manipulatedTable = this.logs.filter(i => {
      for (let filterType of Object.keys(this.filters)) {
        if (filterType != 'date') {
          if (this.filters[filterType] && (!i[filterType] || i[filterType].toLowerCase().indexOf(this.filters[filterType].toLowerCase()) == -1))
            return false;
        }
        else {
          if (this.filters[filterType]) {
            let date = dateFormat(new Date(i.date), 'Date', 'dd/MM/yyyy HH:mm:ss');
            if (date.indexOf(this.filters[filterType]) == -1)
              return false;
          }
        }
      }

      return true;
    });

    this.currentP = 0;
    this.updateTable();
  }

  logInfo: ActionLogs = null;
  onOpenModal(log: ActionLogs) {
    $('#actionLogInfo').modal('show');
    this.logInfo = copy(log);
    this.logInfo['details'] = this.logInfo.description.split(';');
  }
}
