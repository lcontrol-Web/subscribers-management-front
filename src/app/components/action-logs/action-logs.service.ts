import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActionLogs } from 'src/app/interfaces/general';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ActionLogsService {

  constructor(private dataService: DataService) { }

  public getLogs(): Observable<ActionLogs[]> {
    return this.dataService.post('action-logs/get-logs', {});
  }
}
