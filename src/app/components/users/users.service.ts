import { AuthService } from './../../services/auth.service';
import { DataService } from './../../services/data.service';
import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/general';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private dataService: DataService, private authService: AuthService) { }

  public getUsers(): Observable<User[]> {
    return this.dataService.post('users/get-users', {});
  }

  public addUser(user: User): Observable<User> {
    user['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('users/add-user', user)
  }

  public editUser(user: User): Observable<User> {
    user['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('users/edit-user', user)
  }

  public deleteUser(user: { userId: number }): Observable<any> {
    user['operatorId'] = this.authService.currentUser.userId;
    return this.dataService.post('users/delete-user', user)
  }
}
