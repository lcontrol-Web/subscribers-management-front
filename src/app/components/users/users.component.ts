import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/general';

declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  loadingTable = true;

  users: User[] = [];
  manipulatedTable: User[] = [];
  userToEdit: User = null;
  modalType: 'Add' | 'Edit' | 'Delete' = null;

  searchActive = false;

  /* filters */
  filters = {
    userName: '',
    fullName: '',
    email: '',
    phone: ''
  }

  /* sorts */
  sorts = {
    userName: 0,
    fullName: 0,
    email: 0,
    phone: 0
  }

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUsers().subscribe(res => {
      this.users = res;
      this.manipulatedTable = this.users;
      this.loadingTable = false;
    });
  }

  public onOpenModal(type: 'Add' | 'Edit' | 'Delete', user?: User) {
    $('#editCellModal').modal('show');
    this.modalType = type;

    this.userToEdit = user ? copy(user) : {
      userId: null,
      userName: null,
      fullName: null,
      password: null,
      phone: null,
      email: null,
      allowedToCreateUser: 0,
      allowedToCreateSubscriber: 0,
      allowedToUpdateSubscriber: 0,
      allowedToGenerateLicense: 0
    };

    $(document).ready(() => {
      //let passwordInput = $('#customPassword').removeAttr('required');
      if (type == 'Add') {
        setTimeout(() => {
          this.userToEdit.userName = null;
          this.userToEdit.password = null;
        }, 500);
      }
    })
  }

  public loadingRequest = false;
  public passwordEmptyError = false;
  public userNameAlreadyExists = false;

  public onSubmit() {
    this.loadingRequest = true;
    this.passwordEmptyError = false;
    this.userNameAlreadyExists = false;

    if (this.modalType == 'Add') {
      if (!this.userToEdit.password) {
        this.passwordEmptyError = true;
        return;
      }
      this.usersService.addUser(this.userToEdit).subscribe(res => {
        this.users.splice(0, 0, res);
        $('#editCellModal').modal('hide');
      }, (ex) => {
        if (ex.error.indexOf('userName_UNIQUE') != -1)
          this.userNameAlreadyExists = true;
        console.log(ex.error);
      });
    }
    else if (this.modalType == 'Edit') {
      this.usersService.editUser(this.userToEdit).subscribe(res => {
        for (let i = 0; i < this.users.length; i++) {
          if (this.users[i].userId == res.userId) {
            this.users.splice(i, 1, res);
            break;
          }
        }
        $('#editCellModal').modal('hide');
      }, (ex) => {
        if (ex.error.indexOf('userName_UNIQUE') != -1)
          this.userNameAlreadyExists = true;
        console.log(ex.error);
      });
    }
    else {
      this.usersService.deleteUser({ userId: this.userToEdit.userId }).subscribe(res => {
        for (let i = 0; i < this.users.length; i++) {
          if (this.users[i].userId == this.userToEdit.userId) {
            this.users.splice(i, 1);
            break;
          }
        }
        $('#editCellModal').modal('hide');
      });
    }
  }

  onToggleSort(sortType) {
    // reset all others
    for (let sort of Object.keys(this.sorts))
      if (sort != sortType)
        this.sorts[sort] = 0;

    // toggle selected sort
    if (this.sorts[sortType] == 0)
      this.sorts[sortType] = 1;

    else if (this.sorts[sortType] == 1)
      this.sorts[sortType] = -1;

    else if (this.sorts[sortType] == -1)
      this.sorts[sortType] = 0;

    // apply sort
    this.manipulatedTable.sort((a, b) => {
      if (!a[sortType])
        return 1;
      if (!b[sortType])
        return -1;

      if (a[sortType].toString().toLowerCase() >= b[sortType].toString().toLowerCase())
        return this.sorts[sortType];
      return -1 * this.sorts[sortType];
    });
  }

  filterBy() {
    this.manipulatedTable = this.users.filter(i => {
      for (let filterType of Object.keys(this.filters)) {
        if (this.filters[filterType] && (!i[filterType] || i[filterType].toLowerCase().indexOf(this.filters[filterType].toLowerCase()) == -1))
          return false;
      }

      return true;
    });
  }
}
