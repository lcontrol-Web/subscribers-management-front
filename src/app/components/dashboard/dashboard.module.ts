import { UsersComponent } from '../users/users.component';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SubscribersComponent } from '../subscribers/subscribers.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ActionLogsComponent } from '../action-logs/action-logs.component';

@NgModule({
  imports: [
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [
    DashboardComponent,
    SubscribersComponent,
    UsersComponent,
    ActionLogsComponent
  ]
})
export class DashboardModule { }
