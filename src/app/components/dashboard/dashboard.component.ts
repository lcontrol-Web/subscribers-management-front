import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public authService: AuthService, private router:Router) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }

  public openOrCloseNavbar() {
    $("#wrapper").toggleClass("toggled");
  }

  public closeNavigationOnSmallScreens() {
    if (window.innerWidth <= 768)
      $('.visible-xs button').click();
  }

  public onLogout() {
    localStorage.removeItem('currentUser');
    this.authService.currentUser = null;

    this.router.navigate(['']);

  }
}
