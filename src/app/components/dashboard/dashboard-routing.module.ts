import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { SubscribersComponent } from '../subscribers/subscribers.component';
import { UsersComponent } from '../users/users.component';
import { ActionLogsComponent } from '../action-logs/action-logs.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: '',
                redirectTo: 'subscribers'
            },
            {
                path: 'subscribers',
                component: SubscribersComponent,
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'action-logs',
                component: ActionLogsComponent
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class DashboardRoutingModule { }
