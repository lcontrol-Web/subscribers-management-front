import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CorporationService } from 'src/app/services/corporation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  private returnUrl: string; // after re-login redirect to the origin path

  public checkLogin = false; // shows loading when login is clicked
  public wrongLogin = false; // shows error on wrong auth
  public isLoginBtnHidden = true; // show/hide login button

  public userName = '';
  public password = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public corporationData: CorporationService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.router.events.subscribe((val) => {
          // get return url from route parameters or default to '/'
          this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        });
      }
    );

    this.getCorporationData();
  }

  private getCorporationData() {
    this.corporationData.getCorporationsData().subscribe(() => {
      this.isLoginBtnHidden = false;
    });
  }

  login() {
    this.checkLogin = true;
    let data = {
      userName: this.userName,
      password: this.password
    };

    this.authService.login(data).subscribe(res => {
      this.checkLogin = false;

      if (res)
        this.successLogin(res);
      else
        this.wrongLogin = true;
    }, (ex)=>{
      console.log(ex.message);
    });
  }

  private successLogin(user) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.authService.currentUser = user;

    if (this.returnUrl)
      this.router.navigate([this.returnUrl]);
    else {
      this.router.navigate(['/dashboard']);
    }
  }
}
