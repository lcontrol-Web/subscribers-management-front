/***********************   Copy function   ***********************/
declare var copy;
copy = function (param) {
    return JSON.parse(JSON.stringify(param));
}

/***********************   Date functions   ***********************/
declare var dateFormat;
dateFormat = function (value: any, sourceFormat, resultFormat) {
    if (!value || !sourceFormat || !resultFormat) return null;

    let middleFormat = null; // change all sourceFormat to datetime object

    switch (sourceFormat) {
        case 'Date': {
            middleFormat = value;
            break;
        }
        case 'dd/MM/yyyy HH:mm:ss':
        case 'dd-MM-yyyy HH:mm:ss': {
            let d = +(value.substring(0, 2));
            let M = +(value.substring(3, 5));
            let y = +(value.substring(6, 10));
            let h = +(value.substring(11, 13));
            let m = +(value.substring(14, 16));
            let s = +(value.substring(17, 19));

            middleFormat = new Date(y, M - 1, d, h, m, s);
            break;
        }
        case 'yyyy-MM-dd HH:mm:ss': {
            let y = +(value.substring(0, 4));
            let M = +(value.substring(5, 7));
            let d = +(value.substring(8, 10));
            let h = +(value.substring(11, 13));
            let m = +(value.substring(14, 16));
            let s = +(value.substring(17, 19));

            middleFormat = new Date(y, M - 1, d, h, m, s);
            break;
        }
        case 'dd/MM/yyyy': {
            let d = +(value.substring(0, 2));
            let M = +(value.substring(3, 5));
            let y = +(value.substring(6, 10));

            middleFormat = new Date(y, M - 1, d);
            break;
        }
    }

    switch (resultFormat) {
        case 'Date': {
            return middleFormat;
        }
        case 'yyyy-MM-dd HH:mm:ss': {
            return middleFormat.getFullYear() + '-' +
                corrZero(middleFormat.getMonth() + 1) + '-' +
                corrZero(middleFormat.getDate()) + ' ' +
                corrZero(middleFormat.getHours()) + ':' +
                corrZero(middleFormat.getMinutes()) + ':' +
                corrZero(middleFormat.getSeconds());
        }
        case 'dd-MM-yyyy HH:mm:ss': {
            return corrZero(middleFormat.getDate()) + '-' +
                corrZero(middleFormat.getMonth() + 1) + '-' +
                middleFormat.getFullYear() + ' ' +
                corrZero(middleFormat.getHours()) + ':' +
                corrZero(middleFormat.getMinutes()) + ':' +
                corrZero(middleFormat.getSeconds());
        }
        case 'dd/MM/yyyy HH:mm:ss': {
            return corrZero(middleFormat.getDate()) + '/' +
                corrZero(middleFormat.getMonth() + 1) + '/' +
                middleFormat.getFullYear() + ' ' +
                corrZero(middleFormat.getHours()) + ':' +
                corrZero(middleFormat.getMinutes()) + ':' +
                corrZero(middleFormat.getSeconds());
        }
        case 'dd/MM/yyyy': {
            return corrZero(middleFormat.getDate()) + '/' +
                corrZero(middleFormat.getMonth() + 1) + '/' +
                middleFormat.getFullYear();
        }
    }
}

declare var corrZero;
corrZero = function (num) {
    return num >= 10 ? num : '0' + num;
}
