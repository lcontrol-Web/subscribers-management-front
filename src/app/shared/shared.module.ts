import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';

// global functions
import "./globalfunctions1"
import { LoadingComponent } from '../support-components/loading/loading.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CalendarModule,
    ToastModule
  ],
  declarations: [
    LoadingComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ToastModule,
    CalendarModule,

    LoadingComponent
  ]
})
export class SharedModule { }
