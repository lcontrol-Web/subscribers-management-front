const version = '1.0.0.0';
const lastUpdate = '2020-01-13 09:00';

(function (window) {
  window.__env = window.__env || {};
  
  // API url
  window.__env.apiUrl = 'http://localhost:3333/';

}(this));
